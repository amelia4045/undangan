// Get that hamburger menu cookin' //

document.addEventListener("DOMContentLoaded", function() {
  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(function($el) {
      $el.addEventListener("click", function() {
        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);
        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle("is-active");
        $target.classList.toggle("is-active");
      });
    });
  }
});

//Smooth Anchor Scrolling
$(document).on("click", 'a[href^="#"]', function(event) {
  event.preventDefault();
  $("html, body").animate(
    {
      scrollTop: $($.attr(this, "href")).offset().top
    },
    500
  );
});

// When the user scrolls down 20px from the top of the document, show the scroll up button
window.onscroll = function() {
  scrollFunction();
};

$('#toTop')
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("toTop").style.display = "block";
  } else {
    document.getElementById("toTop").style.display = "none";
  }
}
// $(document).ready(function() {
//   /******************************
//       BOTTOM SCROLL TOP BUTTON
//    ******************************/

//   // declare variable
//   var scrollTop = $(".scrollTop");

//   $(window).scroll(function() {
//     // declare variable
//     var topPos = $(this).scrollTop();

//     // if user scrolls down - show scroll to top button
//     if (topPos > 100) {
//       $(scrollTop).css("opacity", "1");

//     } else {
//       $(scrollTop).css("opacity", "0");
//     }

//   }); // scroll END

//   //Click event to scroll to top
//   $(scrollTop).click(function() {
//     $('html, body').animate({
//       scrollTop: 0
//     }, 800);
//     return false;

//   }); 
  // click() scroll top EMD

// Preloader
$(document).ready(function($) {
  var Body = $('body');
  Body.addClass('preloader-site');
  });
  function preloadeSite(){
  
  $('.preloader-wrapper').fadeOut();
  $('body').removeClass('preloader-site');
  }
  
  window.addEventListener('load', preloadeSite);
